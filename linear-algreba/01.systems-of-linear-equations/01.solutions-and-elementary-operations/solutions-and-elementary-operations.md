# NGHIỆM VÀ CÁC PHÉP BIẾN ĐỔI TƯƠNG ĐƯƠNG

Nhiều bài toán thực tế trong các lĩnh vực như sinh học, thương mại, hóa học, khoa học máy tính, kinh tế, điện tử, kỹ thuật, vật lý và khoa học xã hội có thể quy về việc giải một hệ phương trình tuyến tính (*system of linear equations*). Đại số tuyến tính (*linear algebra*) hình thành từ nỗ lực tìm kiếm một phương pháp giải có hệ thống các hệ phương trình tuyến tính. Cho nên bước đầu tiên để hiểu được đại số tuyến tính là tìm hiểu về hệ phương trình tuyến tính.

## Phương trình tuyến tính

Nếu $`a`$, $`b`$ và $`c`$ là các số thực, $`a`$ và $`b`$ không đồng thời bằng không, đồ thị của phương trình

```math
\tag{1} ax + by = c
```

là một đường thẳng và phương trình (1) gọi là **phương trình tuyến tính** (*linear equation*) đối với hai biến $`x`$ và $`y`$ (nghĩa Hán - Việt của *tuyến* là đường thẳng).

Tổng quát, một phương trình có dạng

```math
\tag{2} a_1x_1 + a_2x_2 + ... + a_nx_n = b
```

được gọi là **phương trình tuyến tính** đối với $`n`$ biến $`x_1`$, $`x_2`$, $`...`$ , $`x_n`$. Trong đó $`a_1`$, $`a_2`$, $`...`$, $`a_n`$ là các số thực, gọi là các **hệ số** (*coefficients*) tương ứng của $`x_1`$, $`x_2`$, $`...`$, $`x_n`$ còn $`b`$ cũng là một số thực, gọi là **số hạng không đổi** (*constant term*) hay **số hạng tự do** của phương trình. Chẳng hạn,

```math
\tag{3} 2x_1 - 3x_2 + 5x_3 = 7
```

là một phương trình tuyến tính; các hệ số của $`x_1`$, $`x_2`$, $`x_3`$ lần lượt là $`2`$, $`-3`$ và $`5`$, hệ số tự do là $`7`$.

## Hệ phương trình tuyến tính

Tập hữu hạn các phương trình tuyến tính đối với các biến $`x_1`$, $`x_2`$, $`...`$, $`x_n`$ gọi là một **hệ phương trình tuyến tính** (*system of linear equations*) theo các biến này.

Ví dụ:

```math
\tag{4} \begin{aligned}
x+ y + \enspace z& = 3 \\
2x+ y + 3z& = 1
\end{aligned}
```
là hệ phương trình tuyến tính đối với ba biến $`x`$, $`y`$, $`z`$.

## Nghiệm của hệ phương trình tuyến tính

Cho phương trình tuyến tính $`a_1x_1 + a_2x_2 + ... + a_nx_n = b`$, dãy $`n`$ số $`s_1`$, $`s_2`$, $`...`$, $`s_n`$ gọi là nghiệm của phương trình nếu:

```math
\tag{5} a_1s_1 + a_2s_2 + ... + a_ns_n = b
```

Dãy $`n`$ số $`s_1`$, $`s_2`$, $`...`$, $`s_n`$ là nghiệm của hệ phương trình tuyến tính nếu nó là nghiệm của tất cả các phương trình trong hệ.

Ví dụ, $`x=-2`$, $`y=5`$, $`z=0`$ và $`x=0`$, $`y=4`$, $`z=-1`$ là hai nghiệm của hệ (4).

Một hệ có thể không có nghiệm (vô nghiệm), có một nghiệm duy nhất hoặc có vô số nghiệm. Ví dụ, hệ $`x+y=2`$, $`x+y=3`$ vô nghiệm bởi tổng hai số không thể vừa bằng 2, vừa bằng 3. Hệ vô nghiệm còn gọi là **hệ không nhất quán** (*inconsistent*); hệ có ít nhất một nghiệm gọi là **hệ nhất quán** (*consistent*).

## Biểu diễn nghiệm của hệ theo tham số

Xét bài tập ví dụ sau:

**Ví dụ 1**

Chứng tỏ rằng, với mọi giá trị của $`s`$ và $`t`$,

```math
\tag{6}
\begin{aligned}
&x_1 = t - s + 1 \\
&x_2 = t + s + 2 \\
&x_3 = s \\
&x_4 = t
\end{aligned}
```

là nghiệm của hệ

```math
\tag{7}
\begin{aligned}
x_1 &- 2x_2 + 3x_3 + x_4 =& -3 \\
2x_1 &- \enspace x_2 + 3x_3 - x_4 =& 0
\end{aligned}
```

**Lời giải**. Thế đơn giản các giá trị của $`x_1`$, $`x_2`$, $`x_3`$ và $`x_4`$ vào mỗi phương trình.

```math
\begin{aligned}
x_1 &- 2x_2 + 3x_3 + x_4 = (t-s+1) - 2(t+s+2) + 3s + t = -3 \\
2x_1 &- \enspace x_2 + 3x_3 - x_4 = 2(t-s+1) -(t+s+2) + 3s-t = 0
\end{aligned}
```

Vì cả hai phương trình được thỏa mãn, nên $`x_1`$, $`x_2`$, $`x_3`$ và $`x_4`$ là nghiệm của (7) với mọi giá trị $`s`$ và $`t`$.

Các giá trị $`s`$ và $`t`$ trong (6) được gọi là các **tham số** (parameters), (6) là **dạng tham số** (*parametric form*) của nghiệm, nghiệm cho dưới dạng tham số gọi là **nghiệm tổng quát** (*general solution*) của hệ.

Sau đây là một ví dụ khác mà hệ chỉ có một phương trình duy nhất.

**Ví dụ 2**

Biểu diễn dưới dạng tham số nghiệm của phương trình $`3x-y+2z=6`$.

**Lời giải.** Rút $`y`$ theo $`x`$ và $`z`$ chúng ta có $`y=3x+2z-6`$. Gọi $`s`$ và $`t`$ là hai số bất kỳ, đặt $`x=s`$, $`z=t`$, được

```math
\begin{aligned}
&x=s \\
&y=3s+2t-6 \enspace \text{với \textit{s} và \textit{t} tùy ý} \\ 
&z=t
\end{aligned}
```

là nghiệm viết dưới dạng tham số của phương trình đã cho.

Tất nhiên có thể rút $`x`$ theo $`y`$ và $`z`$ được $`x=\frac{1}{3}(y-2z+6)`$, sau đó đặt $`y=p`$, $`z=q`$ ta có

```math
\begin{aligned}
&x=\frac{1}{3}(y-2z+6) \\
&y=p \qquad\qquad\qquad \text{với \textit{p} và \textit{q} tùy ý} \\
&z=q
\end{aligned}
```

là một biểu diễn tham số khác của nghiệm.

## Biểu diễn hình học nghiệm của hệ

Khi chỉ có hai biến, đồ thị của phương trình $`ax+by=c`$ là một đường thẳng khi $`a`$ và $`b`$ không đồng thời bằng không. Hơn nữa điểm $`P(s,t)`$ thuộc đường thẳng khi và chỉ khi $`as+bt=c`$ - tức $`x=s`$ và $`y=t`$ là một nghiệm của phương trình.

Do đó mỗi nghiệm của một hệ phương trình tuyến tính tương ứng với điểm $`P(s,t)`$ cùng nằm trên tất cả các đồ thị đường thẳng của các phương trình trong hệ.

Cụ thể, nếu hệ chỉ có một phương trình, sẽ có vô số nghiệm, bởi có vô số điểm trên đường thẳng đồ thị. Nếu hệ có hai phương trình, có ba khả năng sau:

1. *Hai đường thẳng cắt nhau tại một điểm. Hệ có một nghiệm duy nhất.*
2. *Hai đường thẳng song song (và phân biệt) với nhau. Hệ phương trình vô nghiệm.*
3. *Hai đường thẳng trùng nhau. Hệ phương trình có vô số nghiệm - là điểm bất kỳ trên đường thẳng chung.*

Ba trường hợp trên được mô tả trong hình dưới đây. Ở trường hợp cuối, hai phương trình $`3x-y=4`$ và $`-6x+2y=-8`$ có đồ thị trùng nhau.

![](images/soluations-and-elementary-operations-1.png)

Khi có ba biến, đồ thị của $`ax+by+cz=d`$ là một mặt phẳng và một lần nữa ta có "hình ảnh" của tập nghiệm từ sự tương giao của các mặt phẳng.

Tuy nhiên phương pháp hình học có giới hạn. Khi có nhiều hơn ba biến, hình ảnh biểu diễn nghiệm của phương trình là một **siêu phẳng** (*hyperplanes*) không thể biểu diễn được trong không gian ba chiều.

